package imptfeatures;


@FunctionalInterface
interface Interface1{
    int add(int a, int b);
    
    static void sub() {
        System.out.println("Numbers will be subtracted");
    }
    default void mult() {
        System.out.println("the numbers will be multiplied");
    }
}
public class Arithmaticoperation
{
    
   public static void main(String[] args) {
       Interface1 i1=(a,b)->
        {
            System.out.println("Addition of a and b is:");
            return(a+b);
        };
        
         System.out.println(i1.add(10,20));
         Interface1.sub();
         i1.mult();
    }}

